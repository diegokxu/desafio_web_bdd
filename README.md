Olá para esse projeto você deverá ter instalado Java JDK 8* para consegui executar o projeto,
você deverá usar uma IDE que suporte linguagem java(recomendo intellij). 
No projeto existe um driver do Chrome, esse driver possibilida abrir o navegador, lembrando que o versão do chromeDriver deverá ser exatamente a versão do seu navegador chrome. 
(caso a versão do driver seja diferente do seu navegador, baixe a versão do chrome driver correspondente a versão chrome do seu navegador e substitua pela versão do projeto)
Foi ultilizado Serenity, Cucumber para gerar relatorios e criar os cénarios em BDD.
Para orquestrar e asserções foi utilizado o Junit.
Para rodar a aplicação basta navegar ate a pasta src/test/java/br/com/inmetrics/desafio/testes e lá existe as classes de testes
basta clicar com o botão direito e da um "Run".


Os relatorios de testes estão em target/site/serenity procure um aquivo index(irá abrir o navegador com o relátorio pronto).