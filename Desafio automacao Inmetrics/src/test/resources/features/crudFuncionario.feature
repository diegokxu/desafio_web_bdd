Feature: Cadastrar Funcionário

Background:
  Given Preciso está logado no sistema

  Scenario: cadastrar funcionario com sucesso
    Given que quero adicionar um novo funcionario
    And preencho todos os dados cadastrais do funcionario
    When envio o cadastro
    Then o funcionario e cadastrado com sucesso


  Scenario: editar o nome do funcionario
    Given que eu quero editar um funcionario
    And quero trocar o nome dele
    When quando salvar a edicao
    Then o funcionario trocara o nome


  Scenario: excluir funcionario
    Given que quero excluir um funcionario aleatorio
    When eu clicar em excluir
    Then o funcionario some da tabela




