package br.com.inmetrics.desafio.pages;

import br.com.inmetrics.desafio.bases.PageBase;
import br.com.inmetrics.desafio.util.ClasseDeUtilidades;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class EditarECadastrarFuncionarioPage extends PageBase {

    public EditarECadastrarFuncionarioPage(WebDriver driver) {
        super(driver);
    }

    By campoNome = By.id("inputNome");
    By campoCPF = By.id("cpf");
    By campoAdmissao = By.id("inputAdmissao");
    By campoCargo = By.id("inputCargo");
    By campoSalario = By.id("dinheiro");
    By radioPj = By.id("pj");
    By campoEnviar = By.className("cadastrar-form-btn");
    By selectSexo = By.id("slctSexo");

    //Açoes
    public void EditarOuCadastrarNome(String nomeEditado){
        clearAndSendKeys(campoNome,nomeEditado);
    }
    public void EditarOuCadastrarCampoCpf(String cpfNovo) throws Exception {
        clearAndSendKeys(campoCPF,cpfNovo);
    }

    public void EditarOuCadastrarCampoAdmissao(String dataAdmissao){
        clearAndSendKeys(campoAdmissao, dataAdmissao);
    }

    public void EditarOuCadastrarCargo(String cargo){
        clearAndSendKeys(campoCargo,cargo);
    }

    public void EditarOuCadastrarSalario(String salarioNovo){
        clearAndSendKeys(campoSalario, salarioNovo);
    }

    public void ClicarEmPj(){
        click(radioPj);
    }

    public void Enviar(){
        click(campoEnviar);
    }

    public void SelecionarSexo(String sexo){

        comboBoxSelectByVisibleText(selectSexo,sexo);
    }

}
