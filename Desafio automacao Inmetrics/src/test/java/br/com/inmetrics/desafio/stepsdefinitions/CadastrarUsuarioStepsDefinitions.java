package br.com.inmetrics.desafio.stepsdefinitions;

import br.com.inmetrics.desafio.pages.CadastrarPage;
import br.com.inmetrics.desafio.pages.MainPage;
import br.com.inmetrics.desafio.util.ClasseDeUtilidades;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;


public class CadastrarUsuarioStepsDefinitions {


    CadastrarPage cadastrarPage;
    MainPage mainPage;
    ClasseDeUtilidades classeDeUtilidades = new ClasseDeUtilidades();


    @Given("^que quero abrir o site da inmetrics$")
    public void quero_cadastrar_um_usuário() {
        mainPage.open();
    }

    @Given("^ao clicar em cadastre-se$")
    public void ao_clicar_em_cadastre_se() {
        mainPage.ClicarEmCadastrar();

    }

    @When("^Preencho os dados cadastrais$")
    public void preencho_os_dados_cadastrais() {
    String usuario = "Diegoss"+classeDeUtilidades.RetornaNumeroRandom();
    String senha = "123456";
    cadastrarPage.EscreverUsuario(usuario);
    cadastrarPage.InformarSenha(senha);
    cadastrarPage.ConfirmarSenha(senha);
    cadastrarPage.ClicarEmCadastrar();

    }

    @Then("^o usuário deverá ser cadastrado com sucesso$")
    public void o_usuário_deverá_ser_cadastrado_com_sucesso() {

        Assert.assertTrue(mainPage.RetornaSeTituloLoginEstaVisivel());

    }
}
