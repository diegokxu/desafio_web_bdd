package br.com.inmetrics.desafio.stepsdefinitions;

import br.com.inmetrics.desafio.pages.EditarECadastrarFuncionarioPage;
import br.com.inmetrics.desafio.pages.MainPage;
import br.com.inmetrics.desafio.pages.MainFuncionarioPage;
import br.com.inmetrics.desafio.util.ClasseDeUtilidades;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.junit.Assert;

import java.util.List;


public class CrudFuncionarioStepsDefinitions {

@Steps
LogarStepsDefinitions logarStepsDefinitions;
MainPage mainPage;
MainFuncionarioPage mainFuncionarioPage;
EditarECadastrarFuncionarioPage editarECadastrarFuncionarioPage;
ClasseDeUtilidades classeDeUtilidades = new ClasseDeUtilidades();
String nome;
int qtd;

    @Given("^Preciso está logado no sistema$")
    public void preciso_está_logado_no_sistema() {
        mainPage.open();
    logarStepsDefinitions.preencho_usuario_e_senha();
    }

    @Given("^que quero adicionar um novo funcionario$")
    public void que_quero_adicionar_um_novo_funcionario() {
        mainFuncionarioPage.ClicarEmNovoFuncionario();
    }

    @Given("^preencho todos os dados cadastrais do funcionario$")
    public void preencho_todos_os_dados_cadastrais_do_funcionario() throws Exception {
    nome = "Diego_"+classeDeUtilidades.RetornaNumeroRandom();

    String dataAdmissao = "01102019";
    String cargo = "Analista_"+classeDeUtilidades.RetornaNumeroRandom();
    String salario = "70000";
    String cpf =classeDeUtilidades.geraCPF();
    String sexo = "Masculino";
    editarECadastrarFuncionarioPage.EditarOuCadastrarNome(nome);
    editarECadastrarFuncionarioPage.EditarOuCadastrarCampoCpf(cpf);
    editarECadastrarFuncionarioPage.EditarOuCadastrarCampoAdmissao(dataAdmissao);
    editarECadastrarFuncionarioPage.EditarOuCadastrarCargo(cargo);
    editarECadastrarFuncionarioPage.EditarOuCadastrarSalario(salario);
    editarECadastrarFuncionarioPage.SelecionarSexo(sexo);
    editarECadastrarFuncionarioPage.ClicarEmPj();

    }

    @When("^envio o cadastro$")
    public void envio_o_cadastro() {
        editarECadastrarFuncionarioPage.Enviar();
    }

    @Then("^o funcionario e cadastrado com sucesso$")
    public void o_funcionario_e_cadastrado_com_sucesso() {
        List<String> registros = mainFuncionarioPage.RetornaElementosNaTabela();
        Assert.assertTrue(registros.contains(nome));
    }


    @Given("^que eu quero editar um funcionario$")
    public void que_eu_quero_editar_um_funcionario() {
       mainFuncionarioPage.EditarFuncionario();
    }

    @Given("^quero trocar o nome dele$")
    public void quero_trocar_o_nome_dele() {
        nome = "NomeEditado_"+classeDeUtilidades.RetornaNumeroRandom();
        editarECadastrarFuncionarioPage.EditarOuCadastrarNome(nome);
    }

    @When("^quando salvar a edicao$")
    public void quando_salvar_a_edicao() {
        editarECadastrarFuncionarioPage.Enviar();
    }

    @Then("^o funcionario trocara o nome$")
    public void o_funcionario_trocara_o_nome() {
        List<String> registros = mainFuncionarioPage.RetornaElementosNaTabela();
        Assert.assertTrue(registros.contains(nome));
    }

    @Given("^que quero excluir um funcionario aleatorio$")
    public void que_quero_excluir_um_funcionario_aleatorio() {

    }

    @When("^eu clicar em excluir$")
    public void eu_clicar_em_excluir() {
        List<String> registros = mainFuncionarioPage.RetornaElementosNaTabela();
        qtd =registros.size();
       mainFuncionarioPage.DeletarFuncionario();
    }

    @Then("^o funcionario some da tabela$")
    public void o_funcionario_some_da_tabela() {
        List<String> registros = mainFuncionarioPage.RetornaElementosNaTabela();
        Assert.assertNotEquals(registros.size(),qtd);
    }
}
