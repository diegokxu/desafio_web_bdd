package br.com.inmetrics.desafio.pages;

import br.com.inmetrics.desafio.bases.PageBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MainPage extends PageBase {


    public MainPage(WebDriver driver) {
        super(driver);
    }

    By campoUsuario = By.xpath("//input[@name='username']");
    By campoSenha = By.xpath("//input[@name='pass']");
    By LinkCadastre = By.xpath("//a[contains(.,'Cadastre-se')]");
    By botaoEntrar = By.xpath("//button[contains(.,'Entre')]");
    By tituloLogin = By.xpath("//span[@class='login100-form-title p-b-1']");

    //Ações
    public void LoginPageOpen(){

    }

    public void InformaUmUsuário(String usuario){
        sendKeys(campoUsuario,usuario);
    }

    public void InformarSenha (String senha){
        sendKeys(campoSenha, senha);
    }

    public void ClicarEmCadastrar(){
        click(LinkCadastre);
    }

    public void ClicarEmEntrar(){
        click(botaoEntrar);
    }

    public boolean RetornaSeTituloLoginEstaVisivel(){
        return  returnIfElementIsDisplayed(tituloLogin);
    }

}