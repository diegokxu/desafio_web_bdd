package br.com.inmetrics.desafio.pages;

import br.com.inmetrics.desafio.bases.PageBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CadastrarPage extends PageBase {

    public CadastrarPage(WebDriver driver) {
        super(driver);

    }

    By campoUsuario = By.name("username");
    By  campoPassword = By.name("pass");
    By campoConfirmaPassword = By.name("confirmpass");
    By botaoCadastrar = By.xpath("//button[contains(.,'Cadastrar')]");


    //Ações
    public void EscreverUsuario (String usuarioCadastrar){
        sendKeys(campoUsuario, usuarioCadastrar);
    }

    public void InformarSenha (String senha){
        sendKeys(campoPassword, senha);
    }

    public void ConfirmarSenha(String senhaconfirmada){
        sendKeys(campoConfirmaPassword, senhaconfirmada);
    }

    public void ClicarEmCadastrar(){
        click(botaoCadastrar);
    }

}
