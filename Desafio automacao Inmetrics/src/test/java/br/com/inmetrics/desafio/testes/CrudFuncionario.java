package br.com.inmetrics.desafio.testes;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = "src/test/resources/features/crudFuncionario.feature" ,
        glue = {"classpath:br.com.inmetrics.desafio.stepsdefinitions"
        })
public class CrudFuncionario {
}
