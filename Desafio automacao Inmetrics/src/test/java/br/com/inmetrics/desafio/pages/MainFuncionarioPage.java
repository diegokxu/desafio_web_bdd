package br.com.inmetrics.desafio.pages;

import br.com.inmetrics.desafio.bases.PageBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class MainFuncionarioPage extends PageBase {

    public MainFuncionarioPage(WebDriver driver) {
        super(driver);
    }


    By linkFuncionario  = By.xpath("//a[contains(.,'Funcionários')]");
    By linkNovoFuncionario = By.xpath("//a[@href='/empregados/new_empregado']");
    By botaoDeletarFuncionario = By.id("delete-btn");
    By botaoEditar = By.xpath("//button[@class='btn btn-warning']");
    By informacaoRegistro = By.id("tabela_info");
    By tabelaRegistro = By.xpath("//tbody");


    //Ações
    public boolean RetornaSeExisteLinkFuncionarios(){
        return returnIfElementIsDisplayed(linkFuncionario);
    }

    public void ClicarEmNovoFuncionario(){
        click(linkNovoFuncionario);
    }

    public void DeletarFuncionario(){
        click(botaoDeletarFuncionario);
    }

    public void EditarFuncionario(){
        click(botaoEditar);
    }

    public List<String>RetornaElementosNaTabela(){
        return  RetornaElemtoDaTabela(tabelaRegistro);
    }

}