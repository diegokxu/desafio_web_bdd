package br.com.inmetrics.desafio.bases;
import net.serenitybdd.core.pages.PageObject;
import org.apache.commons.lang3.time.StopWatch;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class PageBase extends PageObject {

    protected WebDriverWait wait = null;
    protected WebDriver driver = null;
    protected long timeOutDefault;

            public PageBase(WebDriver driver){
            super(driver);
            this.driver = driver;
            timeOutDefault = getWaitForTimeout().toMillis();
            this.wait = new WebDriverWait(driver, timeOutDefault);
            driver.manage().window().maximize();
        }

    protected WebElement waitForElement(By locator){
        wait.until(ExpectedConditions.presenceOfElementLocated(locator));
        WebElement element = driver.findElement(locator);
        wait.until(ExpectedConditions.elementToBeClickable(element));
        return element;
    }

    protected void click(By locator) {
        StopWatch timeOut = new StopWatch();
        timeOut.start();

        while (timeOut.getTime() <= timeOutDefault) {
            WebElement element = null;

            try {
                element = waitForElement(locator);
                element.click();
                timeOut.stop();
                return;
            } catch (Exception e) {
                continue;
            }
        }
    }

    protected void sendKeys(By locator, String text){
        waitForElement(locator).sendKeys(text);
    }

    protected String getText(By locator){
        String text = waitForElement(locator).getText();
        return text;
    }

    protected void clearAndSendKeys(By locator, String text){
        WebElement webElement = waitForElement(locator);
        webElement.sendKeys(Keys.CONTROL + "a");
        webElement.sendKeys(Keys.DELETE);
        webElement.sendKeys(text);
    }

    protected boolean returnIfElementIsDisplayed(By locator){
        boolean result = waitForElement(locator).isDisplayed();
        return result;
    }

    protected List<String> RetornaElemtoDaTabela(By locator){
        List<String> nomeTabela = new ArrayList<String>();
        if(returnIfElementIsDisplayed(locator)){
            WebElement tabela = waitForElement(locator);
            List<WebElement> subTabela = tabela.findElements(By.xpath("//td[@class='text-center sorting_1']"));

            for (WebElement elemento : subTabela) {
                nomeTabela.add(elemento.getText());
            }
        }
        return  nomeTabela;
    }


    protected String RetornaQuantidadeDeItemNaTabela(By locator){
        List<String> nomeTabela = new ArrayList<String>();
        if(returnIfElementIsDisplayed(locator)){
            WebElement tabela = waitForElement(locator);
            tabela.findElements(By.xpath("//td[@class='text-center sorting_1']"));
            return  tabela.getSize().toString();
        }
        return null;
    }

    protected void comboBoxSelectByVisibleText(By locator, String text){
        Select comboBox = new Select(waitForElement(locator));
        comboBox.selectByVisibleText(text);
    }
}
