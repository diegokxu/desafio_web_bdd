package br.com.inmetrics.desafio.hooks;
import br.com.inmetrics.desafio.stepsdefinitions.CadastrarUsuarioStepsDefinitions;
import cucumber.api.java.Before;
import java.net.MalformedURLException;

public class Hooks {
CadastrarUsuarioStepsDefinitions cadastrarUsuarioStepsDefinitions;
        @Before
        public void beforeScenario() throws MalformedURLException {
                cadastrarUsuarioStepsDefinitions.quero_cadastrar_um_usuário();
        }
}