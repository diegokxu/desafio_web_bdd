package br.com.inmetrics.desafio.stepsdefinitions;

import br.com.inmetrics.desafio.pages.MainPage;
import br.com.inmetrics.desafio.pages.MainFuncionarioPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class LogarStepsDefinitions {

    MainPage mainPage;
    MainFuncionarioPage mainFuncionarioPage;

    @Given("^efetuar o login$")
    public void efetuar_o_login() {

    }

    @When("^Preencho usuario e senha$")
    public void preencho_usuario_e_senha() {
        String usuario = "DiegoTeste";
        String senha = "123456";
        mainPage.InformaUmUsuário(usuario);
        mainPage.InformarSenha(senha);
        mainPage.ClicarEmEntrar();

    }

    @Then("^o login e efetuado com sucesso$")
    public void o_login_e_efetuado_com_sucesso() {
        Assert.assertTrue(mainFuncionarioPage.RetornaSeExisteLinkFuncionarios());


    }
}
